#! /bin/bash
#    Copyright (c) 2022
#
#    This file is part of PlymouthOS
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

#Require root privlages
if [[ $UID != 0 ]]
then
  echo "Must be run as root."
  exit
fi

shopt -s dotglob

export PACKAGEOPERATIONLOGDIR=/buildlogs/package_operations

#Create a log folder for the remove operations
mkdir "$PACKAGEOPERATIONLOGDIR"/Removes

#function to handle moving back dpkg redirect files for chroot
function RevertFile {
  TargetFile=$1
  SourceFile=$(dpkg-divert --truename "$1")
  if [[ "$TargetFile" != "$SourceFile" ]]
  then
    rm "$1"
    dpkg-divert --local --rename --remove "$1"
  fi
}

#function to handle temporarily moving files with dpkg that attempt to cause issues with chroot
function RedirectFile {
  RevertFile "$1"
  dpkg-divert --local --rename --add "$1" 
  ln -s /bin/true "$1"
}


if [[ $DEBIAN_DISTRO == Debian ]]
then
  echo "blacklist pcspkr" > /etc/modprobe.d/nobeep.conf
fi

#Set the pager to not be interactive
export PAGER=cat

#Copy the import files into the system, while creating a deb with checkinstall.
cp /usr/import/tmp/* /tmp
cd /tmp
mkdir debian
touch debian/control
#remove any old deb files for this package
rm "/srcbuild/buildoutput/"plyos-plyos_*.deb
/usr/import/usr/libexec/build_core/checkinstall -y -D --fstrans=no --nodoc --dpkgflags=--force-overwrite --install=yes --backup=no --pkgname=plyos-plyos --pkgversion=1 --pkgrelease=$(date +%s)  --maintainer=plyos@plyos --pkgsource=plyos --pkggroup=plyos --requires="" /tmp/configure_phase3_helper.sh
cp *.deb "/srcbuild/buildoutput/"
cd $OLDPWD


#copy all files
rsync /usr/import/* -Ka /
chmod 777 /tmp

#Redirect grub-install if lupin isn't 1st tier
if [[ ! -e /usr/share/initramfs-tools/hooks/lupin_casper ]]
then
  dpkg-divert --add --rename --divert /usr/sbin/grub-install.real /usr/sbin/grub-install
  echo 'if [ -x /usr/sbin/grub-install.lupin ]; then /usr/sbin/grub-install.lupin "$@"; else /usr/sbin/grub-install.real "$@"; fi; exit $?' > /usr/sbin/grub-install
  chmod +x /usr/sbin/grub-install
fi


#clean apt stuff
apt-get clean
rm -rf /var/cache/apt-xapian-index/*
rm -rf /var/lib/apt/lists/*
rm -rf /var/lib/dlocate/*

#run the script that calls all compile scripts in a specified order, in build only mode
compile_all build-only

#Append the snapshot date to the end of the revisions file
cat /tmp/APTFETCHDATE >> /usr/share/buildcore_revisions.txt

#start the PWD in the build dir, so that when GDB is started, the relative paths in the source are correct
echo "cd /srcbuild/plymouth/build" >> /root/.bashrc
echo "set horizontal-scroll-mode on" >> /root/.inputrc

echo 'if [[ $(tty) == /dev/ttyS* || $(tty) == /dev/hvc* ]]' >> /root/.bashrc
echo 'then                         ' >> /root/.bashrc
echo '  sleep .1                   ' >> /root/.bashrc
echo '  stty cols 160              ' >> /root/.bashrc
echo 'fi                           ' >> /root/.bashrc

#Enable 4 serial gettys
mkdir -p /etc/systemd/system/getty@tty1.service.d
echo "[Service]" >> /etc/systemd/system/getty@tty1.service.d/override.conf
echo "ExecStart=" >> /etc/systemd/system/getty@tty1.service.d/override.conf
echo "ExecStart=-/sbin/agetty -o '-p -f -- \\u' --autologin root --noclear %I linux" >> /etc/systemd/system/getty@tty1.service.d/override.conf


mkdir -p /etc/systemd/system/serial-getty@ttyS0.service.d
echo "[Service]" > /etc/systemd/system/serial-getty@ttyS0.service.d/override.conf
echo "ExecStart=" >> /etc/systemd/system/serial-getty@ttyS0.service.d/override.conf
echo "ExecStart=-/sbin/agetty -o '-p -f -- \\u' --autologin root --keep-baud 115200,57600,38400,9600 %I linux" >> /etc/systemd/system/serial-getty@ttyS0.service.d/override.conf
systemctl enable serial-getty@ttyS0.service

mkdir -p /etc/systemd/system/serial-getty@ttyS1.service.d
echo "[Service]" > /etc/systemd/system/serial-getty@ttyS1.service.d/override.conf
echo "ExecStart=" >> /etc/systemd/system/serial-getty@ttyS1.service.d/override.conf
echo "ExecStart=-/sbin/agetty -o '-p -f -- \\u' --autologin root --keep-baud 115200,57600,38400,9600 %I linux" >> /etc/systemd/system/serial-getty@ttyS1.service.d/override.conf
systemctl enable serial-getty@ttyS1.service

mkdir -p /etc/systemd/system/serial-getty@ttyS2.service.d
echo "[Service]" > /etc/systemd/system/serial-getty@ttyS2.service.d/override.conf
echo "ExecStart=" >> /etc/systemd/system/serial-getty@ttyS2.service.d/override.conf
echo "ExecStart=-/sbin/agetty -o '-p -f -- \\u' --autologin root --keep-baud 115200,57600,38400,9600 %I linux" >> /etc/systemd/system/serial-getty@ttyS2.service.d/override.conf
systemctl enable serial-getty@ttyS2.service

mkdir -p /etc/systemd/system/serial-getty@ttyS3.service.d
echo "[Service]" > /etc/systemd/system/serial-getty@ttyS3.service.d/override.conf
echo "ExecStart=" >> /etc/systemd/system/serial-getty@ttyS3.service.d/override.conf
echo "ExecStart=-/sbin/agetty -o '-p -f -- \\u' --autologin root --keep-baud 115200,57600,38400,9600 %I linux" >> /etc/systemd/system/serial-getty@ttyS3.service.d/override.conf
systemctl enable serial-getty@ttyS3.service

mkdir -p /etc/systemd/system/serial-getty@hvc0.service.d
echo "[Service]" > /etc/systemd/system/serial-getty@hvc0.service.d/override.conf
echo "ExecStart=" >> /etc/systemd/system/serial-getty@hvc0.service.d/override.conf
echo "ExecStart=-/sbin/agetty -o '-p -f -- \\u' --autologin root --keep-baud 115200,57600,38400,9600 %I linux" >> /etc/systemd/system/serial-getty@hvc0.service.d/override.conf
systemctl enable serial-getty@hvc0.service

mkdir -p /etc/systemd/system/serial-getty@hvc1.service.d
echo "[Service]" > /etc/systemd/system/serial-getty@hvc1.service.d/override.conf
echo "ExecStart=" >> /etc/systemd/system/serial-getty@hvc1.service.d/override.conf
echo "ExecStart=-/sbin/agetty -o '-p -f -- \\u' --autologin root --keep-baud 115200,57600,38400,9600 %I linux" >> /etc/systemd/system/serial-getty@hvc1.service.d/override.conf
systemctl enable serial-getty@hvc1.service

mkdir -p /etc/systemd/system/serial-getty@hvc2.service.d
echo "[Service]" > /etc/systemd/system/serial-getty@hvc2.service.d/override.conf
echo "ExecStart=" >> /etc/systemd/system/serial-getty@hvc2.service.d/override.conf
echo "ExecStart=-/sbin/agetty -o '-p -f -- \\u' --autologin root --keep-baud 115200,57600,38400,9600 %I linux" >> /etc/systemd/system/serial-getty@hvc2.service.d/override.conf
systemctl enable serial-getty@hvc2.service

mkdir -p /etc/systemd/system/serial-getty@hvc3.service.d
echo "[Service]" > /etc/systemd/system/serial-getty@hvc3.service.d/override.conf
echo "ExecStart=" >> /etc/systemd/system/serial-getty@hvc3.service.d/override.conf
echo "ExecStart=-/sbin/agetty -o '-p -f -- \\u' --autologin root --keep-baud 115200,57600,38400,9600 %I linux" >> /etc/systemd/system/serial-getty@hvc3.service.d/override.conf
systemctl enable serial-getty@hvc3.service

#Create a default /etc/vconsole.conf for plymouth
echo "XKBLAYOUT=\"us\"" >> /etc/vconsole.conf
echo "XKBMODEL=\"pc105\"" >> /etc/vconsole.conf
echo "XKBVARIANT=\"\"" >> /etc/vconsole.conf
echo "XKBOPTIONS=\"compose:lwin\"" >> /etc/vconsole.conf

echo "git config --global --add safe.directory /srcbuild/plymouth" >> /etc/profile

echo "/opt/sbin/plymouthd --mode=boot --kernel-command-line=splash --attach-to-session --pid-file=/run/plymouth/pid --debug --no-daemon & sleep .5; plymouth show-splash; fg 1" > /root/.bash_history

#Actions that are performed after all the packages are compiled
function PostInstallActions
{
  #copy all files again to ensure that the SVN versions are not overwritten by a checkinstalled version
  rsync /usr/import/* -Ka /

  #move the import folder
  mv /usr/import /tmp

  #Add nls modules to the initramfs
  echo -e '#!/bin/sh\n. /usr/share/initramfs-tools/hook-functions\ncopy_modules_dir kernel/fs/nls' > /usr/share/initramfs-tools/hooks/nlsmodules
  chmod 755 /usr/share/initramfs-tools/hooks/nlsmodules
  
  #Force initramfs utilites to include the overlay filesystem
  echo overlay >> /etc/initramfs-tools/modules

  (. /usr/bin/build_vars; . /usr/bin/wlruntime_vars; ldconfig)

  (. /usr/bin/build_vars; . /usr/bin/wlruntime_vars; /opt/sbin/plymouth-set-default-theme spinfinity)

  #save the build date of the CD.
  echo "$(date)" > /etc/builddate
}
PostInstallActions |& tee -a "$PACKAGEOPERATIONLOGDIR"/PostInstallActions.log

#start the remastersys job
(. /usr/bin/build_vars; remastersys dist)
mv /home/remastersys/remastersys/custom.iso /home/remastersys/custom.iso
rm -rf /home/remastersys/remastersys/*
