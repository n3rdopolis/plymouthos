#! /bin/bash
#    Copyright (c) 2022
#
#    This file is part of PlymouthOS
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

#enable Job control
set -m

INSTANCEID=$(date +%s)
INSTANCEPORT=$(shuf -i 32768-60999 -n 1)

mkdir -p /tmp/plymouthos/$INSTANCEID
qemu-system-x86_64 -m 4096 -cdrom ~/PlymouthOS_amd64.iso -spice port=$INSTANCEPORT,disable-ticketing=on -enable-kvm -display gtk -device virtio-vga,xres=1024,yres=768,edid=on,max_outputs=2 \
-device secondary-vga,xres=1024,yres=768,edid=on \
\
-chardev socket,id=serial0,path=/tmp/plymouthos/$INSTANCEID/plyserial0,server=on,wait=off -serial chardev:serial0 \
-chardev socket,id=serial1,path=/tmp/plymouthos/$INSTANCEID/plyserial1,server=on,wait=off -serial chardev:serial1 \
-chardev socket,id=serial2,path=/tmp/plymouthos/$INSTANCEID/plyserial2,server=on,wait=off -serial chardev:serial2 \
-chardev socket,id=serial3,path=/tmp/plymouthos/$INSTANCEID/plyserial3,server=on,wait=off -serial chardev:serial3 \
-chardev socket,id=serial4,path=/tmp/plymouthos/$INSTANCEID/plyserial4,server=on,wait=off -serial chardev:serial4 \
-chardev socket,id=serial5,path=/tmp/plymouthos/$INSTANCEID/plyserial5,server=on,wait=off -serial chardev:serial5 \
-chardev socket,id=serial6,path=/tmp/plymouthos/$INSTANCEID/plyserial6,server=on,wait=off -serial chardev:serial6 \
-chardev socket,id=serial7,path=/tmp/plymouthos/$INSTANCEID/plyserial7,server=on,wait=off -serial chardev:serial7 \
\
-device virtio-serial \
-device virtconsole,chardev=serial4,name=serial4 \
-device virtconsole,chardev=serial5,name=serial5 \
-device virtconsole,chardev=serial6,name=serial6 \
-device virtconsole,chardev=serial7,name=serial7 \
\
-device ich9-usb-ehci1,id=usb \
-device ich9-usb-uhci1,masterbus=usb.0,firstport=0,multifunction=on \
-device ich9-usb-uhci2,masterbus=usb.0,firstport=2 \
-device ich9-usb-uhci3,masterbus=usb.0,firstport=4 \
\
-chardev spicevmc,name=usbredir,id=usbredirchardev1 \
-device usb-redir,chardev=usbredirchardev1,id=usbredirdev1 \
-chardev spicevmc,name=usbredir,id=usbredirchardev2 \
-device usb-redir,chardev=usbredirchardev2,id=usbredirdev2 \
-chardev spicevmc,name=usbredir,id=usbredirchardev3 \
-device usb-redir,chardev=usbredirchardev3,id=usbredirdev3 \
-chardev spicevmc,name=usbredir,id=usbredirchardev4 \
-device usb-redir,chardev=usbredirchardev4,id=usbredirdev4 &

if [[ ! -z $PLYMOUTHOS_DEBUG_INPUT_DEVICE && $PLYMOUTHOS_DEBUG_INPUT_DEVICE != 0 ]]
then
  remote-viewer spice://127.0.0.1:$INSTANCEPORT &
fi

echo "\
    title: VM: /dev/ttyS0 (%n);; command: socat -,raw,escape=0x1d,echo=0 unix-connect:/tmp/plymouthos/$INSTANCEID/plyserial0
    title: VM: /dev/ttyS1 (%n);; command: socat -,raw,escape=0x1d,echo=0 unix-connect:/tmp/plymouthos/$INSTANCEID/plyserial1
    title: VM: /dev/ttyS2 (%n);; command: socat -,raw,escape=0x1d,echo=0 unix-connect:/tmp/plymouthos/$INSTANCEID/plyserial2
    title: VM: /dev/ttyS3 (%n);; command: socat -,raw,escape=0x1d,echo=0 unix-connect:/tmp/plymouthos/$INSTANCEID/plyserial3
    title: VM: /dev/hvc0 (%n);; command: socat -,raw,escape=0x1d,echo=0 unix-connect:/tmp/plymouthos/$INSTANCEID/plyserial4
    title: VM: /dev/hvc1 (%n);; command: socat -,raw,escape=0x1d,echo=0 unix-connect:/tmp/plymouthos/$INSTANCEID/plyserial5
    title: VM: /dev/hvc2 (%n);; command: socat -,raw,escape=0x1d,echo=0 unix-connect:/tmp/plymouthos/$INSTANCEID/plyserial6
    title: VM: /dev/hvc3 (%n);; command: socat -,raw,escape=0x1d,echo=0 unix-connect:/tmp/plymouthos/$INSTANCEID/plyserial7" > /tmp/plymouthos/$INSTANCEID/plymouthos_tabs
konsole --tabs-from-file /tmp/plymouthos/$INSTANCEID/plymouthos_tabs -e true &
fg 1 > /dev/null

if [[ -e /tmp/plymouthos/$INSTANCEID/plyserial0 ]]; then   rm /tmp/plymouthos/$INSTANCEID/plyserial0; fi
if [[ -e /tmp/plymouthos/$INSTANCEID/plyserial1 ]]; then   rm /tmp/plymouthos/$INSTANCEID/plyserial1; fi
if [[ -e /tmp/plymouthos/$INSTANCEID/plyserial2 ]]; then   rm /tmp/plymouthos/$INSTANCEID/plyserial2; fi
if [[ -e /tmp/plymouthos/$INSTANCEID/plyserial3 ]]; then   rm /tmp/plymouthos/$INSTANCEID/plyserial3; fi
if [[ -e /tmp/plymouthos/$INSTANCEID/plyserial4 ]]; then   rm /tmp/plymouthos/$INSTANCEID/plyserial4; fi
if [[ -e /tmp/plymouthos/$INSTANCEID/plyserial5 ]]; then   rm /tmp/plymouthos/$INSTANCEID/plyserial5; fi
if [[ -e /tmp/plymouthos/$INSTANCEID/plyserial6 ]]; then   rm /tmp/plymouthos/$INSTANCEID/plyserial6; fi
if [[ -e /tmp/plymouthos/$INSTANCEID/plyserial7 ]]; then   rm /tmp/plymouthos/$INSTANCEID/plyserial7; fi
if [[ -e /tmp/plymouthos/$INSTANCEID/plymouthos_tabs ]]; then   rm /tmp/plymouthos/$INSTANCEID/plymouthos_tabs; fi
if [[ -d /tmp/plymouthos/$INSTANCEID ]]; then   rm -d /tmp/plymouthos/$INSTANCEID; fi
