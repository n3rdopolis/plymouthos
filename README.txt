LICENSE:
  The build script are all under GPL v2

OVERVIEW:
  This generates images that are designed to make it easy to test and develop plymouth
  

  plymouthos_files/usr/share/initramfs-tools/scripts/init-bottom/plytest
  can be edited to test plymouth commands at boot time
  
  Run plymouthos_makediff.sh specifying a path to a plymouth git repo, it generates plymouthos_files/usr/share/PLYOS_PATCHES/plymouth.diff
  with all the changes which is then applied in the git repo compiled in the build chroot

  specify 'plymouth.debuginitramfs' as a kernel argument to get the virtual serial consoles to break to a prompt while Plymouth is still running from the initramfs
  
BUILDING:
  Run
  plymouthos_builder.sh
  and press enter two times. It makes two chroots under 
  /var/cache/PLYOS_Build_Files/
  and places the ISO as
  ~/PlymouthOS_${BUILDARCH}.iso
  Usually ~/PlymouthOS_amd64.iso

  Plymouth is set to unconditionally rebuild each build, as plymouthos_builder removes the control file that would prevent rebuilds otherwise

  The Configure log (Meson) is generated to /var/cache/PLYOS_Build_Files/logs/latest-amd64/build_core/plymouth/PackagePrepareBuild
  The Compile log of Plymouth is generated to /var/cache/PLYOS_Build_Files/logs/latest-amd64/build_core/plymouth/PackageCompileSource
  The Install log of Plymouth is generated to /var/cache/PLYOS_Build_Files/logs/latest-amd64/build_core/plymouth/PackageInstallSource

  - Add tests to plymouthos_files/usr/bin and make them executable as needed

  CONTROL FILES (relative to /var/cache/PLYOS_Build_Files):
        DontDownloadDebootstrapScript:                  Delete this file to force the downloaded debootstrap in PLYOS_Build_Files to run again at the
                                                        next build
        DontRestartArchivesamd64:                       Delete this file to force all the downloaded packages to be downloaded again for the 
                                                        respective architecture.
        DontRestartSourceDownloadamd64:                 Delete this file to force all the downloaded source repositories to be downloaded again for
                                                        the respective architecture.
        DontRestartPhase1amd64:                         Delete this file to force Phase1 to debootstrap again for the respective architecture. This
                                                        only hosts the smaller chroot system that downloads everything
        DontRestartPhase2amd64:                         Delete this file to force Phase2 to debootstrap again for the respective architecture. This
                                                        is the chroot that gets copied to Phase3, and is on the output ISO files.
        DontRestartBuildoutputamd64:                    Delete this file to force all deb packages to rebuild for the respective architecture. This
                                                        will increase the build time.
        DontRestartRustDownloadamd64:                   Delete this file to force build_core to re-download Rust
        DontStartFromScratchamd64:                      Delete this file to force delete everything included downloaded repositories for the
                                                        respective architecture, and cause it to start from scratch.
        DontRestartCargoDownloadamd64:                  Clear the Cargo cache
        DontRestartRustDownloadamd64:                   Force build_core to download a new build of Rust
        build/amd64/buildoutput/control/(packagename):  Delete these files to specify a specific package to rebuild.
        buildcore_revisions_amd64.txt:                  Add a revisions file into this path, to specify particular packages, as described above
        RestartPackageList_amd64.txt:                   Add in the list of packages (as in the files in build/amd64/buildoutput/control/ ).
                                                        One per each line. For batch resetting particular packages
        DontForceSnapshotBuildamd64:                    Delete this file only after the first run is complete, before the next build. This forces
                                                        temporary chroots to be built
USING:
  Run
  plymouthos_runner.sh
  It needs socat, konsole, and qemu-system-x86_64, and remote-viewer is needed for PLYMOUTHOS_DEBUG_INPUT_DEVICE mode
  It runs socat in 8 tabs of Konsole that connect to the 8 serial ports on the VM
  
  To debug hot add/remove input devices, run 'export PLYMOUTHOS_DEBUG_INPUT_DEVICE=1' to get plymouthos_runner to start the remote-viewer that allows dynamic USB device forwarding
