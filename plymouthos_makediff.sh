#! /bin/bash
#    Copyright (c) 2022
#
#    This file is part of PlymouthOS
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

ScriptPath=$(dirname $(readlink -f "$0"))

if [[ -z "$1" ]]
then
  echo "Must specify a path to a (plymouth) git repo"
  exit 1
fi

DiffRepoPath=$(readlink -f "$1")
if [[ ! -d "$DiffRepoPath/.git" ]]
then
  echo "$DiffRepoPath is not a git repo. Must specify a path to a (plymouth) git repo"
  exit 1
fi

env -C "$DiffRepoPath" -- git remote show difforigin &> /dev/null
RemoteQueryReturn=$?
if [[ $RemoteQueryReturn != 0 ]]
then
  env -C "$DiffRepoPath" -- git remote add difforigin "https://gitlab.freedesktop.org/plymouth/plymouth.git/"
fi
env -C "$DiffRepoPath" -- git fetch difforigin
env -C "$DiffRepoPath" -- git diff --binary difforigin/main > "$ScriptPath/plymouthos_files/usr/share/PLYOS_PATCHES/plymouth.diff"

#https://stackoverflow.com/questions/855767/can-i-use-git-diff-on-untracked-files
for UntrackedFile in $(env -C "$DiffRepoPath" -- git ls-files --others --exclude-standard )
do
  env -C "$DiffRepoPath" -- git diff --binary --no-index /dev/null "$UntrackedFile" >> "$ScriptPath/plymouthos_files/usr/share/PLYOS_PATCHES/plymouth.diff"
done
